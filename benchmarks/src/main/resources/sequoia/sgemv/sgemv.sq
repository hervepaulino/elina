/*******************************************************************************
 * Copyright (c) 2010,
 *   The Board of Trustees of The Leland Stanford Junior University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * 
 * 3. The names of the contributors may not be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "sgemv.h"

// NOTE:
// This implementation does not block the vector x.
// It assumes that x will always fit in the LS.
// More importantly, it assumes that both x and a couple of rows of A will fit in the LS
//   simultaneously so that no reduction is required.

task<inner> void sgemv(in float a[M][N], in float x[N], inout float y[M], in float alpha)
{
	tunable Mblk;
	tunable Nblk;  
	mappar ( int i = 0 : M/Mblk )
	{
		mapseq( int j = 0 : N/Nblk )
		{
	  		sgemv(a[i*Mblk;Mblk][j*Nblk;Nblk], x[j*Nblk;Nblk], y[i*Mblk;Mblk], alpha);
		}
	}
}

task<leaf> void sgemv(in float a[M][N], in float x[N], inout float y[M], in float alpha)
{
	for ( unsigned int i = 0; i < M; i++ )
	{
		float temp = 0.0;
		for ( unsigned int j = 0; j < N; j++ )
			temp += a[i][j] * x[j];
		y[i] = alpha * temp + y[i]; 
	}
}

void reference(float** a, float* x, float* y, unsigned int M, unsigned int N, float alpha)
{
	for ( unsigned int i = 0; i < M; i++ )
	{
		float temp = 0.0;
		for ( unsigned int j = 0; j < N; j++ )
			temp += a[i][j] * x[j];
		y[i] = alpha * temp + y[i]; 
	}
}
