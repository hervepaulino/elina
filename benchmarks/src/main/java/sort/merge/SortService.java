package sort.merge;

import service.IService;


public interface SortService extends IService {

	int[] mergeSort(int[] vector);
}
