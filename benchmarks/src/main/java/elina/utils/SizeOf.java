package elina.utils;

public class SizeOf {

	public static int Byte = 1;
	public static int Char = 2;
	
	public static int Short = 2;
	public static int Int = 4;
	public static int Long = 8;
	
	public static int Float = 4;
	public static int Double = 8;	
	
	public static int Boolean = 1;  // not precisely specified 		
}
