package drivers;

import service.IMonitor;

public interface MonitorDriver {
	
	IMonitor createMonitor();
}
