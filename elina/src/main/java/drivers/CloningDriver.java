package drivers;

public interface CloningDriver {
	<T> T copy(T object);	
}
