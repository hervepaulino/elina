package drivers;


public interface HierarchyWriteDriver {

	boolean createHierarchyFile();
	
	boolean createHierarchyFile(String filename);

}
