package service.aggregator;



/**
 * 
 * @author João Saramago
 * @since Dec 6, 2011 4:08:47 PM
 * @param <T>
 */
public interface IServiceScheduler {

	int getIndex();

	void setSize(int length);

	
}
