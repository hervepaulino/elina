package service;

public class NoSuchMethodException extends RuntimeException {

	private static final long serialVersionUID = -6222133695846752146L;

	public NoSuchMethodException(String message)
	{
		super(message);
	}
	
}
