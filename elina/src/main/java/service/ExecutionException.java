package service;

public class ExecutionException 
extends Exception{

	private static final long serialVersionUID = -2619800495482313670L;
	
	public ExecutionException(String message)
	{
		super(message);
	}
}
