package core.collective;

public interface DistributionsCombination {

	Distribution<?>[] getDistributions();
	
	int[][] getCombinations();
	
}
