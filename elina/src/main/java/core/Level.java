package core;

public enum Level {
	Processor,
	Node,
	Cluster,
	InterCluster
}
