package core.init;

public class ConfigurationException 
extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7132519097258376955L;

	public ConfigurationException(String message)
	{
		super(message);
	}
	
}
